//
//  NewsManager.swift
//  NewsViews
//
//  Created by Rezwan Islam on 8/14/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import Foundation
import Alamofire

class NewsManager {
    
    let APIKey:String
    let baseURL:URL
    var newsSources:[NewsSource] = []
    var newsArticles:[NewsArticle] = []
    
    static var shared: NewsManager = {
        let sharedNewsManager = NewsManager(APIKey: "6b1099dce0534abcb39252c65da8bb62", baseURL: "https://newsapi.org/v1/sources?language=en&country=us")
        return sharedNewsManager
    }()
    
    private init(APIKey: String, baseURL: String  ) {
        self.APIKey = APIKey
        self.baseURL = URL(string: baseURL)!
    }
    
    func fetchNews() {
        
        Alamofire.request(baseURL).responseJSON { (response) in
            
            if let responseDict = response.result.value as? [String : Any] {
                if let sourcesDict = responseDict["sources"] as? [Any] {
                    for source in sourcesDict{
                        self.newsSources.append(NewsSource(sourceDictionary: source as! [String : Any]))
                    }
                }
            }
            
            self.newsArticles = []
            for source in self.newsSources {
                self.getArticles(from: source, completionHandler: { (newsArticles) in
                    self.newsArticles.append(contentsOf: newsArticles)
                })
            }
        }
        
    }
    
    private func getArticles(from source: NewsSource, completionHandler: @escaping ([NewsArticle]) -> ()) {
        var articles:[NewsArticle] = []
        if let requestURL = URL(string: "https://newsapi.org/v1/articles?source=\(source.id!)&apiKey=\(self.APIKey)") {
            Alamofire.request(requestURL).responseJSON(completionHandler: { (response) in
                if let responseDict = response.result.value as? [String : Any] {
                    if let articlesArrayDict = responseDict["articles"] as? [Any] {
                        for articleDict in articlesArrayDict {
                            let article = NewsArticle(articleDictionary: articleDict as! [String : Any], source: source.name ?? "")
                            articles.append(article)
                        }
                    }
                }
                completionHandler(articles)
            })
        }
        
    }
    
}
