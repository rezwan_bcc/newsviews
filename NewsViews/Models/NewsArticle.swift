//
//  NewsArticle.swift
//  NewsViews
//
//  Created by Rezwan Islam on 8/14/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import Foundation

class NewsArticle {
    
    let author:String?
    let title:String?
    let description:String?
    let newsURL:String?
    let imageURL:String?
    let date:String?
    let source:String?
    
    struct Keys {
        static let author = "author"
        static let title = "title"
        static let description = "description"
        static let newsURL = "url"
        static let imageURL = "urlToImage"
        static let date = "publishedAt"
    }
    
    init(articleDictionary: [String: Any], source: String) {
        author = articleDictionary[Keys.author] as? String
        title = articleDictionary[Keys.title] as? String
        description = articleDictionary[Keys.description] as? String
        newsURL = articleDictionary[Keys.newsURL] as? String
        imageURL = articleDictionary[Keys.imageURL] as? String
        date = articleDictionary[Keys.date] as? String
        self.source = source
    }
    
}
