//
//  NewsSource.swift
//  NewsViews
//
//  Created by Rezwan Islam on 8/14/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import Foundation

class NewsSource {
    
    let id:String?
    let name:String?
    
    struct Keys {
        static let id = "id"
        static let name = "name"
    }
    
    init(sourceDictionary: [String: Any]) {
        self.id = sourceDictionary[Keys.id] as? String
        self.name = sourceDictionary[Keys.name] as? String
    }
    
}
