//
//  PageVC.swift
//  NewsViews
//
//  Created by Rezwan Islam on 8/11/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import UIKit

class IntroPageViewController: UIPageViewController {
    
    var pages:[PageTemplateViewController] = []
    var pageControl:UIPageControl?
    var nextIndex:Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftPage = self.storyboard?.instantiateViewController(withIdentifier: "PageLeftViewController") as! PageLeftViewController
        leftPage.pageIndex = 0
        
        let centerPage = self.storyboard?.instantiateViewController(withIdentifier: "PageCenterViewController") as! PageCenterViewController
        centerPage.pageIndex = 1
        
        let rightPage = self.storyboard?.instantiateViewController(withIdentifier: "PageRightViewController") as! PageRightViewController
        rightPage.pageIndex = 2
        
        pages = [leftPage, centerPage, rightPage]
        self.setViewControllers([pages[1]], direction: .forward, animated: true, completion: nil)
        self.dataSource = self
        self.delegate = self
        
        addCommonItems()
        
        //disabling page bounce effect
        for view in self.view.subviews {
            if view is UIScrollView {
                (view as! UIScrollView).delegate = self
                break
            }
        }
    }
    
    func addCommonItems() {
        
        let skipIntroButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        skipIntroButton.addTarget(self, action: #selector(skipButtonTapped(_:)), for: .touchUpInside)
        skipIntroButton.translatesAutoresizingMaskIntoConstraints = false
        skipIntroButton.setTitle(" Skip Intro ", for: .normal)
        skipIntroButton.backgroundColor = UIColor.darkGray
        skipIntroButton.layer.cornerRadius = 15
        self.view.addSubview(skipIntroButton)
        let btnCenterXConstraint = NSLayoutConstraint(item: skipIntroButton, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
        let btnBottomYConstraint = NSLayoutConstraint(item: skipIntroButton, attribute: .top, relatedBy: .equal, toItem: self.view, attribute: .top, multiplier: 1, constant: 50)
        NSLayoutConstraint.activate([btnCenterXConstraint, btnBottomYConstraint])
        
        if let pageControl = pageControl {
            self.view.addSubview(pageControl)
            pageControl.isHidden = false
            let centerXConstraint = NSLayoutConstraint(item: pageControl, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1, constant: 0)
            let bottomYConstraint = NSLayoutConstraint(item: pageControl, attribute: .bottom, relatedBy: .equal, toItem: self.view, attribute: .bottom, multiplier: 1, constant: -10)
            //            let heightConstraint = NSLayoutConstraint(item: pageControl, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
            //            let widthConstraint = NSLayoutConstraint(item: pageControl, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 50)
            NSLayoutConstraint.activate([centerXConstraint, bottomYConstraint])
            pageControl.currentPage = pages.count/2
            pageControl.numberOfPages = pages.count
        }
    }
    
    @IBAction func skipButtonTapped(_ sender:UIButton) {
        UserDefaults.standard.set(true, forKey: "PageAlreadyDisplayed")
        self.dismiss(animated: true, completion: nil)
    }
    
    func viewControllerAtIndex(index: Int) -> PageTemplateViewController? {
        if index == NSNotFound || index < 0 || index >= pages.count {
            return nil
        } else {
            return pages[index]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension IntroPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! PageTemplateViewController).pageIndex
        index -= 1
        return self.viewControllerAtIndex(index: index)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        var index = (viewController as! PageTemplateViewController).pageIndex
        index += 1
        return self.viewControllerAtIndex(index: index)
    }

}

extension IntroPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let currentPageVC = pageViewController.viewControllers?.first as! PageTemplateViewController
        if completed {
                pageControl?.currentPage = currentPageVC.pageIndex
        }
    }
    
}

// uiscrollview delegate for disabling page bounce
extension IntroPageViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let currentPageIndex = pageControl?.currentPage
        if currentPageIndex == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentPageIndex == pages.count-1 && scrollView.contentOffset.x >= scrollView.bounds.size.width {
            scrollView.contentOffset = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let currentPageIndex = pageControl?.currentPage
        if currentPageIndex == 0 && scrollView.contentOffset.x <= scrollView.bounds.size.width {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0)
        } else if currentPageIndex == pages.count-1 && scrollView.contentOffset.x >= scrollView.bounds.size.width {
            targetContentOffset.pointee = CGPoint(x: scrollView.bounds.size.width, y: 0)
        }
    }
    
}
