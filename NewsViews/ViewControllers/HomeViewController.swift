//
//  HomeVC.swift
//  NewsViews
//
//  Created by Rezwan Islam on 8/10/17.
//  Copyright © 2017 Rezwan Islam. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.showIntroPage()
    }
    
    func showIntroPage() {
        let alreadyDisplayed = UserDefaults.standard.bool(forKey: "PageAlreadyDisplayed")
        if !alreadyDisplayed {
            let introPageVC = self.storyboard?.instantiateViewController(withIdentifier: "IntroPageViewController") as! IntroPageViewController
            introPageVC.pageControl = pageControl
            self.present(introPageVC, animated: false, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
